<%-- 
    Document   : jstlStrona
    Created on : 2021-12-10, 10:48:22
    Author     : student
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
 <f:view>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
    </head>
    <body>
       <c:import url="WEB-INF/ludzie.xml" var="baza" />
        <x:parse doc="${baza}" var="wynik"/>
        <ol>
<x:forEach select="$wynik/ludzie/czlowiek" var="zasob">
 <li><b><x:out select="nazwisko" /></b>
 <x:out select="imie" /></li>
</x:forEach>
</ol>
        <h:form id="form_logout">
            <h:outputText value="Kliknij aby sie wylogowac" /><br />
            <h:commandButton value="Wyloguj" action="logout" />
            </h:form>
    </body>
</html>
</f:view>
